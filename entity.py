import pygame
from config import *
from math import floor
from spritesheet import get_spritesheet

class Entity(pygame.sprite.Sprite):
  def __init__(self, position, tile_index, dimension = None):
    pygame.sprite.Sprite.__init__(self)
    spritesheet = get_spritesheet()
    self.spritesheet = spritesheet
    
    w = spritesheet.config['tilewidth']
    h = spritesheet.config['tileheight']
    if dimension == None:
      dimension = (w, h)

    self.dimension = dimension

    self.rect = pygame.Rect(position[0], position[1], dimension[0], dimension[1])
    self.animation = None
    self.__animation_running = False
  
    if isinstance(tile_index, dict):
      self.animations = tile_index
      self.set_animation('idle', 0.1)
      self.start_animation()
    else: 
      self.image = spritesheet.get_tile_surface(tile_index, dimension)
      
  def set_animation(self, name, speed = 0.1):
    animation = self.animations[name]
    self.animation = animation
    self.animation_speed = speed
    self.animation_index = 0
    self.image = self.spritesheet.get_tile_surface(animation[0])

  def stop_animation(self):
    self.__animation_running = False
  
  def start_animation(self):
    self.__animation_running = True

  def update(self):
    if self.__animation_running:
      self.image = self.spritesheet.get_tile_surface(self.animation[floor(self.animation_index)])
      self.animation_index += self.animation_speed
      if self.animation_index >= len(self.animation):
        self.animation_index = 0
    super().update()
    
  def draw(self, surface):
    surface.blit(self.image, self.rect)
