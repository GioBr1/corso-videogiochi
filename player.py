import events
from object import Object

class Player(Object):
  def __init__(self, level, position):
    Object.__init__(self, level, position, 19, 1, (17,21))
  
  def update(self):
    self.action = None
    if events.left:
      self.action = 'go-left'
    elif events.right: 
      self.action = 'go-right'
    super().update()
