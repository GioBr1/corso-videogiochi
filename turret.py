from pygame import *
from object import Object
from bullet import Bullet
from lib.collisions import *

#todo provare a rimuovere il rect e ad usare una sprite. in questo modo dovrei poter usare anche i metodi gia' fatti per le collisioni
class Turret(Object):
  def __init__(self, level, position, action, direction):
    Object.__init__(self, level, position, 111, 1, (20, 21))
    self.action = action
    self.direction = direction
    self.range = 6
    self.reloading_time = 1
    self.__reloading_timer = 0
      
  def get_sight_rect(self):
    s = self.spritesheet
    layers = self.level.layers
    turret_x = self.rect[0]
    turret_y = self.rect[1]
    tile_w = s.config['tilewidth']
    tile_h = s.config['tileheight']
    
    sight_width = self.range * tile_w

    for i in range(self.range):
      offset = i * tile_w
      if self.direction == 'left':
        sight_tile_x = self.rect[0] - offset
      else:
        sight_tile_x = self.rect[0] + offset
      
      for e in layers['platforms']:
        if e.rect[1] != turret_y: continue
        element_x = e.rect[0]
        if self.direction == 'left' and element_x >= sight_tile_x and element_x < turret_x:
          return Rect(element_x + tile_w, turret_y, offset - tile_w, tile_h)    
          
        if self.direction == 'right' and  element_x <= sight_tile_x and element_x > turret_x:
          return Rect(turret_x + tile_w, turret_y, offset - tile_w, tile_h)    
    
    if self.direction == 'left':
      left = turret_x - sight_width
      width = sight_width
      if left < 0:
        left = 0
        width = left + turret_x
      return Rect(left, turret_y, width, tile_h)
    else:
      return Rect(turret_x + tile_w, turret_y, sight_width, tile_h)
 
  def fire(self):
    if (self.action == 'reloading'): return 
    
    if self.direction == 'left':
      starting_x = self.rect[0] - self.dimension[0]
    else:
      starting_x = self.rect[0] + self.dimension[0]

    Bullet(self.level, (starting_x, self.rect[1]), 'go-' + self.direction )
    self.action = 'reloading'
          
  def update(self):
    super().update()
    
    target = group_collide_rect(self.get_sight_rect(), self.level.layers['mobiles'])
    if target != None: self.fire()
    
    if self.action == 'reloading':
      self.__reloading_timer += 1 / 60
      if self.__reloading_timer >= self.reloading_time:
        self.action = None
        self.__reloading_timer = 0
  
  def draw(self, surface):
    surface.fill((255,0,0), self.get_sight_rect())
    super().draw(surface)
