from pygame import *
import sys

up = down = left = right = space = next_level = prev_level = False

def listen():
  global up, down, left, right, space, next_level, prev_level
  next_level = prev_level = False
  
  for e in event.get():
    if e.type == QUIT: sys.exit()
    
    if e.type == KEYDOWN:
      if e.key == K_ESCAPE: sys.exit()
      if e.key == K_UP: up = True
      if e.key == K_DOWN: down = True
      if e.key == K_LEFT: left = True
      if e.key == K_RIGHT: right = True
      if e.key == K_SPACE: space = True
        
      if e.key == K_a: next_level = True
      if e.key == K_z: prev_level = True
      
    
    if e.type == KEYUP:
      if e.key == K_ESCAPE: sys.exit()
      if e.key == K_UP: up = False
      if e.key == K_DOWN: down = False
      if e.key == K_LEFT: left = False
      if e.key == K_RIGHT: right = False
      if e.key == K_SPACE: space = False
