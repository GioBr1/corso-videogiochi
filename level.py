import pygame
import json
from config import *
from os import path
from entity import Entity
from player import Player
from spritesheet import get_spritesheet
from object import Object
from turret import Turret
from walker import Walker

class Level():
  def __init__(self, name):
    self.name = name
    spritesheet = get_spritesheet()
    self.spritesheet = spritesheet
    
    with open(path.join(LEVELS_PATH, name + '.json')) as f:
      config = json.load(f)
      self.config = config
    
    layers = {
      'background': pygame.sprite.Group(),
      'back_decorations': pygame.sprite.Group(),
      'platforms': pygame.sprite.Group(),
      'objects': pygame.sprite.Group(),
      'mobiles': pygame.sprite.Group(),
      'front_decorations': pygame.sprite.Group()
    }
    self.layers = layers

    self.ordered_layers = [
      layers['background'],
      layers['back_decorations'],
      layers['platforms'],
      layers['objects'],
      layers['front_decorations'],
      layers['mobiles']
    ]    
    
    for layer_config in config['layers']:
      layer_name = layer_config['name']
      if layer_name in self.layers and layer_name != 'obj':
        layer = self.layers[layer_name]
        
        w = layer_config['width']
        x = 0
        y = 0
        
        for spritesheet_index in layer_config['data']:
          if spritesheet_index != 0:  
            spritesheet_index -= 1
            
            if str(spritesheet_index) in spritesheet.config['tiles'] and 'animation' in spritesheet.config['tiles'][str(spritesheet_index)]:
              tile_index = { 'idle': [] }
              for frame_index in spritesheet.config['tiles'][str(spritesheet_index)]['animation']:
                tile_index['idle'].append(frame_index['tileid'])
            else:
              tile_index = spritesheet_index
  
            layer.add(Entity((x*TILE_SIZE, y*TILE_SIZE), tile_index))
          
          x += 1
          if x == w:
            x = 0
            y += 1
                    
    layers['mobiles'].add(Player(self, (100,10)))
    layers['mobiles'].add(Walker(self, (140,10), 'go-right'))
    layers['mobiles'].add(Walker(self, (240,10), 'go-left'))
    layers['mobiles'].add(Turret(self, (21*15,10), 'watch', 'right'))
    layers['mobiles'].add(Turret(self, (21*11,10), 'watch', 'left'))

  def update(self):
    for layer in self.ordered_layers:
      layer.update()

  def draw(self, surface, layer = None):
    if layer != None:
      self.layers[layer].draw(surface)
    else:  
      for layer in self.ordered_layers:
        for e in layer:
          e.draw(surface)          
