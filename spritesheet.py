from lib.spritesheet import Spritesheet
from config import SPRITESHEET

__spritesheet = None

def get_spritesheet():
  global __spritesheet
  if __spritesheet == None: __spritesheet = Spritesheet(SPRITESHEET)
  return __spritesheet
