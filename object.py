import pygame
from entity import Entity
from math import floor
from config import *

class Object(Entity):
  def __init__(self, level, position, tile_index, speed = 1, dimension = None ):
    Entity.__init__(self, position, tile_index, dimension )
    self.xvel = 0
    self.yvel = 0
    self.onGround = False
    self.fly = False
    self.level = level
    self.speed = speed
    self.action = None
    self.status = None
  
  def get_tile_coords(self):
    return (floor(self.rect.left / TILE_SIZE), floor(self.rect.top / TILE_SIZE))
  
  def is_there_a_platform(self, tile_coords):
    platforms = self.level.layers['platforms']
    for p in platforms:
      if p.rect.left == tile_coords[0] * TILE_SIZE and p.rect[1] == tile_coords[1] * TILE_SIZE:
        return p
    return None
  
  def update(self): 
    self.status = None
    action = self.action
    if action == 'go-left':
      self.xvel = -self.speed
    elif action == 'go-right':
      self.xvel = self.speed
    if not (action == 'go-left' or action == 'go-right'): 
      self.xvel = 0
        
    if not self.onGround and not self.fly:
      # only accelerate with gravity if in the air
      self.yvel += 0.3
      # max falling speed
      if self.yvel > 100: self.yvel = 100
        
    # increment in x direction
    self.rect.left += self.xvel
    # do x-axis collisions
    self.collide(self.xvel, 0, self.level.layers['platforms'])
    # increment in y direction
    self.rect.top += self.yvel
    # assuming we're in the air
    self.onGround = False
    # do y-axis collisions
    
    self.collide(0, self.yvel, self.level.layers['platforms'])
    super().update()
      
  def collide(self, xvel, yvel, platforms):
    for p in platforms:
      if pygame.sprite.collide_rect(self, p):
        if xvel > 0:
          self.rect.right = p.rect.left
          self.status = 'colliding-right'
        if xvel < 0:
          self.rect.left = p.rect.right
          self.status = 'colliding-left'
        if yvel > 0:
          self.rect.bottom = p.rect.top
          self.onGround = True
          self.yvel = 0
        if yvel < 0:
          self.rect.top = p.rect.bottom
