from object import Object
from config import *

class Bullet(Object):
  def __init__(self, level, position, action):
    Object.__init__(self, level, position, 138, 1, (17, 21))
    self.fly = True
    self.speed = 10
    self.action = action
    level.layers['mobiles'].add(self)
  
  def update(self):
    super().update()
    if self.status == 'colliding-right' or self.status == 'colliding-left' or self.rect[0] < 0 or self.rect[0] > SCREEN_SIZE_PX[0] :
      self.kill()
