import pygame
import events
from config import *
from level import Level

def main():
  pygame.init()
  
  screen = pygame.display.set_mode(SCREEN_SIZE_X2_PX, SCREEN_MODE)
  original_size_surface = Surface(SCREEN_SIZE_PX)
  pygame.display.set_caption(WIN_CAPTION)
  
  level_index = 0
  levels = [Level('test')]

  timer = pygame.time.Clock()

  while True:
    timer.tick(60)

    events.listen()
    
    #for x in range(0, SCREEN_SIZE_TL[0]):
    #  for y in range(0, SCREEN_SIZE_TL[1]):
    #    spritesheet.blit((19, 7), screen, (x*TILE_SIZE, y*TILE_SIZE))
    
    if events.next_level:
      level_index += 1
      print(level_index)

    elif events.prev_level:
      level_index -= 1
      print(level_index)
      
    levels[0].update()
    levels[0].draw(original_size_surface)
    
    pygame.transform.scale2x(original_size_surface, screen)
    pygame.display.update()
    
if __name__ == "__main__":
  main()
