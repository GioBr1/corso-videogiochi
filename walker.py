import events
from object import Object

class Walker(Object):
  def __init__(self, level, position, action):
    Object.__init__(self, level, position, 79, 1, (17, 21))
    self.action = action
        
  def update(self):
    if self.status=='colliding-right':
      self.action = 'go-left'
    elif self.status == 'colliding-left':
      self.action = 'go-right'
    super().update()

