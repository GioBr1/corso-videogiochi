def group_collide_rect(rect, group):
  for entity in group:
    colliding_entity = entity_collide_rect(rect, entity)
    if colliding_entity != None: return colliding_entity
      
  return None

def entity_collide_rect(rect, entity):
  e_rect = entity.rect
  
  if rect.left < e_rect.right and \
     rect.right > e_rect.left and \
     rect.top < e_rect.bottom and \
     rect.bottom > e_rect.top:
    return entity
  
  return None
