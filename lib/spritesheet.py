from os import path
from math import floor
from config import *
import json

class Spritesheet:
  def __init__(self, name):
    with open(path.join(ASSETS_PATH, name + '.json')) as f:
      self.config = json.load(f)
    
    self.sheet = image.load(path.join(ASSETS_PATH, path.basename(self.config["image"])))
    self.sheet.convert_alpha()

  def __get_tile_rect(self, tile_index, dimension = None):
    config = self.config

    y = int(floor((tile_index) / self.config['columns']))
    x = tile_index - self.config['columns'] * y
    m = config['margin']
    s = config['spacing']
    w = config['tilewidth']
    h = config['tileheight']
    if dimension == None:
      dimension = w, h
    
    startX = m + x * (w + s)
    startY = m + y * (h + s)
    return Rect(
      startX + (w - dimension[0]) / 2,
      startY + (h - dimension[1]) / 2,
      dimension[0], 
      dimension[1]
    )

  def get_tile_surface(self, tile_index, dimension = None):
    config = self.config
    w = config['tilewidth']
    h = config['tileheight']
    if dimension == None:
      dimension = w, h
        
    tile_surface = Surface(dimension, SRCALPHA) 
    tile_surface.blit(self.sheet, (0,0), self.__get_tile_rect(tile_index, dimension))
    return tile_surface

  def blit(self, tile_index, dest_surface, dimension = None):
    dest_surface.blit(self.sheet, (0,0), self.__get_tile_rect(tile_index, dimension))
    return dest_surface
